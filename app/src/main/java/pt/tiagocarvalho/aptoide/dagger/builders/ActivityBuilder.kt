package pt.tiagocarvalho.aptoide.dagger.builders

import dagger.Module
import dagger.android.ContributesAndroidInjector
import pt.tiagocarvalho.aptoide.ui.main.MainActivity
import pt.tiagocarvalho.aptoide.ui.main.MainActivityModule

@Module
abstract class ActivityBuilder {

    @ContributesAndroidInjector(modules = [MainActivityModule::class])
    abstract fun contributeMainActivity(): MainActivity

}