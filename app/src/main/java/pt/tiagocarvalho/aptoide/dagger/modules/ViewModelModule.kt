package pt.tiagocarvalho.aptoide.dagger.modules

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import pt.tiagocarvalho.aptoide.dagger.ViewModelKey
import pt.tiagocarvalho.aptoide.ui.home.HomeFragmentViewModel
import pt.tiagocarvalho.aptoide.utils.ViewModelProviderFactory


@Module
abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(HomeFragmentViewModel::class)
    abstract fun bindUserViewModel(homeFragmentViewModel: HomeFragmentViewModel): ViewModel

    @Binds
    internal abstract fun bindViewModelFactory(factory: ViewModelProviderFactory): ViewModelProvider.Factory
}