package pt.tiagocarvalho.aptoide.dagger.components

import android.app.Application
import dagger.BindsInstance
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule
import pt.tiagocarvalho.aptoide.AptoideApp
import pt.tiagocarvalho.aptoide.dagger.builders.ActivityBuilder
import pt.tiagocarvalho.aptoide.dagger.modules.AppModule
import javax.inject.Singleton

@Singleton
@Component(
        modules = [
            AndroidSupportInjectionModule::class,
            AppModule::class,
            ActivityBuilder::class
        ]
)
interface AppComponent {

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent

    }

    fun inject(mainApp: AptoideApp)

}