package pt.tiagocarvalho.aptoide.dagger.modules

import dagger.Module
import dagger.Provides
import pt.tiagocarvalho.aptoide.BuildConfig
import pt.tiagocarvalho.aptoide.SchedulerProvider
import pt.tiagocarvalho.aptoide.data.remote.AppService
import pt.tiagocarvalho.aptoide.utils.AppSchedulerProvider
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module(includes = [ViewModelModule::class])
class AppModule {

    @Provides
    @Singleton
    fun provideSchedulerProvider(): SchedulerProvider {
        return AppSchedulerProvider()
    }

    @Singleton
    @Provides
    fun provideAppService(): AppService {
        return Retrofit.Builder()
                .baseUrl(BuildConfig.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
                .create(AppService::class.java)
    }

}