package pt.tiagocarvalho.aptoide.repository

import pt.tiagocarvalho.aptoide.data.remote.AppService
import pt.tiagocarvalho.aptoide.model.ApiResponse
import pt.tiagocarvalho.aptoide.model.Response
import pt.tiagocarvalho.aptoide.testing.OpenForTesting
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
@OpenForTesting
class AppRepository @Inject constructor(
        private val appService: AppService
) {

    fun getAllApps() = appService.listApps()
            .map { response: Response -> ApiResponse(response = response) }
            .onErrorReturn { t: Throwable -> ApiResponse(errorMessage = t.message) }

}