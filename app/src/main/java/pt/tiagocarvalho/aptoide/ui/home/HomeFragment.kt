package pt.tiagocarvalho.aptoide.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingComponent
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import dagger.android.support.AndroidSupportInjection
import pt.tiagocarvalho.aptoide.adapters.EditorsChoiceAdapter
import pt.tiagocarvalho.aptoide.adapters.LocalTopAppsAdapter
import pt.tiagocarvalho.aptoide.binding.FragmentDataBindingComponent
import pt.tiagocarvalho.aptoide.databinding.HomeFragmentBinding
import pt.tiagocarvalho.aptoide.testing.OpenForTesting
import javax.inject.Inject

@OpenForTesting
class HomeFragment : Fragment(), HomeFragmentNavigator {

    companion object {
        fun newInstance(): HomeFragment = HomeFragment()
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    lateinit var viewModel: HomeFragmentViewModel

    lateinit var binding: HomeFragmentBinding

    var dataBindingComponent: DataBindingComponent = FragmentDataBindingComponent(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidSupportInjection.inject(this)
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = HomeFragmentBinding.inflate(inflater, container, false, dataBindingComponent)
        val editorsChoiceAdapter = EditorsChoiceAdapter(dataBindingComponent) { listItem ->
            Toast.makeText(context, listItem.name, Toast.LENGTH_SHORT).show()
        }
        val localTopAppsAdapter = LocalTopAppsAdapter(dataBindingComponent) { listItem ->
            Toast.makeText(context, listItem.name, Toast.LENGTH_SHORT).show()
        }
        binding.rvEditorsChoice.adapter = editorsChoiceAdapter
        binding.rvTopApps.adapter = localTopAppsAdapter
        binding.navigator = this

        subscribeUi(editorsChoiceAdapter, localTopAppsAdapter)
        return binding.root
    }

    private fun subscribeUi(
            editorsChoiceAdapter: EditorsChoiceAdapter,
            localTopAppsAdapter: LocalTopAppsAdapter
    ) {
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(HomeFragmentViewModel::class.java)

        viewModel.errorMessage.observe(viewLifecycleOwner, Observer { errorMessage ->
            if (errorMessage != null) showError(errorMessage)
        })

        viewModel.editorsChoiceApps.observe(viewLifecycleOwner, Observer { result ->
            editorsChoiceAdapter.submitList(result)
        })
        viewModel.localTopApps.observe(viewLifecycleOwner, Observer { result ->
            localTopAppsAdapter.submitList(result)
        })
    }

    private fun showError(errorMessage: String) {
        Toast.makeText(context, errorMessage, Toast.LENGTH_SHORT).show()
    }

    override fun onClickEditorsChoiceMore() {
        Toast.makeText(context, "Open More Editors Choice Apps", Toast.LENGTH_SHORT).show()
    }

    override fun onClickLocalTopAppsMore() {
        Toast.makeText(context, "Open More Local Top Apps", Toast.LENGTH_SHORT).show()
    }


}