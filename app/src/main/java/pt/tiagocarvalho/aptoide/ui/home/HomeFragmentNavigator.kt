package pt.tiagocarvalho.aptoide.ui.home

interface HomeFragmentNavigator {

    fun onClickEditorsChoiceMore()

    fun onClickLocalTopAppsMore()

}