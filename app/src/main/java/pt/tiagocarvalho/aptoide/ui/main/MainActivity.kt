package pt.tiagocarvalho.aptoide.ui.main

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import pt.tiagocarvalho.aptoide.R
import pt.tiagocarvalho.aptoide.databinding.ActivityMainBinding
import pt.tiagocarvalho.aptoide.ui.base.BaseActivity
import pt.tiagocarvalho.aptoide.ui.home.HomeFragment
import javax.inject.Inject

class MainActivity : BaseActivity<ActivityMainBinding>(), HasSupportFragmentInjector {

    private lateinit var navigation: BottomNavigationView

    private var binding: ActivityMainBinding? = null

    @Inject
    lateinit var fragmentInjector: DispatchingAndroidInjector<Fragment>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = mViewDataBinding

        setup()
        initFragment()
    }

    private fun setup() {
        navigation = binding!!.navigation
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
        setSupportActionBar(binding!!.toolbar)
    }

    private fun initFragment() {
        val homeFragment = HomeFragment.newInstance()
        openFragment(homeFragment)
    }


    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        super.onCreateOptionsMenu(menu)
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            R.id.action_user_profile -> makeToast(R.string.show_profile)
        }
        return true
    }

    override fun getLayoutId(): Int {
        return R.layout.activity_main
    }

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.navigation_home -> {
                val homeFragment = HomeFragment.newInstance()
                openFragment(homeFragment)
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_search -> {
                makeToast(R.string.title_search)
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_stores -> {
                makeToast(R.string.title_stores)
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_apps -> {
                makeToast(R.string.title_apps)
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    private fun openFragment(fragment: Fragment) {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.coordinator, fragment)
        transaction.commit()
    }

    override fun supportFragmentInjector(): AndroidInjector<Fragment> = fragmentInjector

    private fun makeToast(res: Int) {
        Toast.makeText(this, getString(res), Toast.LENGTH_SHORT).show()
    }

}
