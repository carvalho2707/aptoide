package pt.tiagocarvalho.aptoide.ui.main

import dagger.Module
import dagger.android.ContributesAndroidInjector
import pt.tiagocarvalho.aptoide.ui.home.HomeFragment

@Module
abstract class MainActivityModule {

    @ContributesAndroidInjector
    abstract fun contributeHomeFragment(): HomeFragment

}