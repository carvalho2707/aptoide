package pt.tiagocarvalho.aptoide.ui.home

import androidx.lifecycle.MutableLiveData
import pt.tiagocarvalho.aptoide.SchedulerProvider
import pt.tiagocarvalho.aptoide.model.ListItem
import pt.tiagocarvalho.aptoide.repository.AppRepository
import pt.tiagocarvalho.aptoide.testing.OpenForTesting
import pt.tiagocarvalho.aptoide.ui.base.BaseViewModel
import javax.inject.Inject

/**
 * I've used 2 different variables just because in a real app each list uses a different remote source
 */
@OpenForTesting
class HomeFragmentViewModel @Inject constructor(val appRepository: AppRepository, val schedulerProvider: SchedulerProvider) : BaseViewModel() {


    val errorMessage: MutableLiveData<String> = MutableLiveData()
    val editorsChoiceApps: MutableLiveData<List<ListItem>> = MutableLiveData()
    val localTopApps: MutableLiveData<List<ListItem>> = MutableLiveData()

    init {
        loadPosts()
    }

    private fun loadPosts() {
        compositeDisposable.add(
                appRepository.getAllApps()
                        .subscribeOn(schedulerProvider.io())
                        .observeOn(schedulerProvider.ui())
                        .subscribe { result ->
                            when {
                                result.hasError() -> {
                                    result.errorMessage?.let {
                                        errorMessage.postValue(it)
                                    }
                                }
                                result.hasData() -> {
                                    result.getData().let {
                                        editorsChoiceApps.postValue(it)
                                    }
                                }
                            }
                        }
        )

        compositeDisposable.add(
                appRepository.getAllApps()
                        .subscribeOn(schedulerProvider.io())
                        .observeOn(schedulerProvider.ui())
                        .subscribe { result ->
                            when {
                                result.hasError() -> {
                                    result.errorMessage?.let {
                                        errorMessage.postValue(it)
                                    }
                                }
                                result.hasData() -> {
                                    result.getData().let {
                                        localTopApps.postValue(it)
                                    }
                                }
                            }
                        }
        )
    }
}