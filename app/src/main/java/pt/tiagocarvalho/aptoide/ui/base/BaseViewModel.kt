package pt.tiagocarvalho.aptoide.ui.base

import androidx.lifecycle.DefaultLifecycleObserver
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable

abstract class BaseViewModel : ViewModel(), DefaultLifecycleObserver {

    var compositeDisposable: CompositeDisposable

    init {
        this.compositeDisposable = CompositeDisposable()
    }

    override fun onCleared() {
        compositeDisposable.dispose()
        super.onCleared()
    }


    override fun onStart(owner: LifecycleOwner) {
        if (compositeDisposable.isDisposed)
            compositeDisposable = CompositeDisposable()
    }

    override fun onDestroy(owner: LifecycleOwner) {
        onCleared()
    }
}