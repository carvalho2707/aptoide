package pt.tiagocarvalho.aptoide.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingComponent
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import pt.tiagocarvalho.aptoide.R
import pt.tiagocarvalho.aptoide.databinding.LocalAppItemBinding
import pt.tiagocarvalho.aptoide.model.ListItem

class LocalTopAppsAdapter(
        private val dataBindingComponent: DataBindingComponent,
        private val onClickCallback: ((ListItem) -> Unit)?
) : ListAdapter<ListItem, LocalTopAppsAdapter.ViewHolder>(
        object : DiffUtil.ItemCallback<ListItem>() {
            override fun areItemsTheSame(oldItem: ListItem, newItem: ListItem): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: ListItem, newItem: ListItem): Boolean {
                return oldItem.id == newItem.id
            }
        }
) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = DataBindingUtil.inflate<LocalAppItemBinding>(
                LayoutInflater.from(parent.context),
                R.layout.local_app_item,
                parent,
                false,
                dataBindingComponent)
        binding.root.setOnClickListener {
            binding.listItem?.let {
                onClickCallback?.invoke(it)
            }
        }
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position))
    }


    class ViewHolder(private val binding: LocalAppItemBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(item: ListItem) {
            binding.apply {
                listItem = item
                executePendingBindings()
            }
        }
    }
}