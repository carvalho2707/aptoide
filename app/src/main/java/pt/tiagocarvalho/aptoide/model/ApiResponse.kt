package pt.tiagocarvalho.aptoide.model

/**
 * This class along with all model entities don't work as expected since the message format may
 * or may not change in case of an error or another scenario
 */
class ApiResponse(val response: Response? = null, val errorMessage: String? = null) {

    fun hasError(): Boolean {
        return errorMessage != null
                || !response?.status?.equals("OK")!!
                || response.responses?.listApps?.datasets?.all?.data?.list == null
    }

    fun hasData(): Boolean {
        return !hasError() && !response?.responses?.listApps?.datasets?.all?.data?.list.isNullOrEmpty()
    }

    fun getData() = response?.responses?.listApps?.datasets?.all?.data?.list as List<ListItem>

}