package pt.tiagocarvalho.aptoide.model

import com.google.gson.annotations.SerializedName

data class Info(

        @field:SerializedName("time")
        val time: Time? = null,

        @field:SerializedName("status")
        val status: String? = null
)