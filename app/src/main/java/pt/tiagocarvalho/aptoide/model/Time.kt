package pt.tiagocarvalho.aptoide.model

import com.google.gson.annotations.SerializedName

data class Time(

        @field:SerializedName("seconds")
        val seconds: Double? = null,

        @field:SerializedName("human")
        val human: String? = null
)