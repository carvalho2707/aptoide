package pt.tiagocarvalho.aptoide.model

import com.google.gson.annotations.SerializedName

data class Datasets(

        @field:SerializedName("all")
        val all: All? = null
)