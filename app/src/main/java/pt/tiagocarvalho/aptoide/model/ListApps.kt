package pt.tiagocarvalho.aptoide.model

import com.google.gson.annotations.SerializedName

data class ListApps(

        @field:SerializedName("datasets")
        val datasets: Datasets? = null,

        @field:SerializedName("info")
        val info: Info? = null
)