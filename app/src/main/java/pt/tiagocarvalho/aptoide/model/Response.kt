package pt.tiagocarvalho.aptoide.model

import com.google.gson.annotations.SerializedName

data class Response(

        @field:SerializedName("responses")
        val responses: Responses? = null,

        @field:SerializedName("status")
        val status: String? = null
)