package pt.tiagocarvalho.aptoide.model

import com.google.gson.annotations.SerializedName

data class Data(

        @field:SerializedName("next")
        val next: Int? = null,

        @field:SerializedName("total")
        val total: Int? = null,

        @field:SerializedName("offset")
        val offset: Int? = null,

        @field:SerializedName("hidden")
        val hidden: Int? = null,

        @field:SerializedName("limit")
        val limit: Int? = null,

        @field:SerializedName("list")
        val list: List<ListItem?>? = null
)