package pt.tiagocarvalho.aptoide.model

import com.google.gson.annotations.SerializedName

data class Responses(

        @field:SerializedName("listApps")
        val listApps: ListApps? = null
)