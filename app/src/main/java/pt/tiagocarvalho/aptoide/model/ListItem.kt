package pt.tiagocarvalho.aptoide.model

import com.google.gson.annotations.SerializedName

/**
 * In this entity I've decided to remove unused attributes for simplicity
 *
 */
data class ListItem(

        @field:SerializedName("rating")
        val rating: Double? = null,

        @field:SerializedName("icon")
        val icon: String? = null,

        @field:SerializedName("name")
        val name: String? = null,

        @field:SerializedName("id")
        val id: Int? = null,

        @field:SerializedName("graphic")
        val graphic: String? = null
)