package pt.tiagocarvalho.aptoide.model

import com.google.gson.annotations.SerializedName

data class All(

        @field:SerializedName("data")
        val data: Data? = null,

        @field:SerializedName("info")
        val info: Info? = null
)