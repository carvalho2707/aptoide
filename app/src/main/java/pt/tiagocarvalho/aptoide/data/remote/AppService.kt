package pt.tiagocarvalho.aptoide.data.remote

import io.reactivex.Observable
import pt.tiagocarvalho.aptoide.model.Response
import retrofit2.http.GET

interface AppService {

    @GET("6/bulkRequest/api_list/listApps")
    fun listApps(): Observable<Response>

}