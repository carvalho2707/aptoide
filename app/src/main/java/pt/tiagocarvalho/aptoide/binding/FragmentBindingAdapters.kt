package pt.tiagocarvalho.aptoide.binding

import android.graphics.drawable.Drawable
import android.widget.ImageView
import androidx.cardview.widget.CardView
import androidx.databinding.BindingAdapter
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.SimpleTarget
import com.bumptech.glide.request.transition.Transition
import pt.tiagocarvalho.aptoide.R
import pt.tiagocarvalho.aptoide.testing.OpenForTesting
import javax.inject.Inject

@OpenForTesting
class FragmentBindingAdapters @Inject constructor(val fragment: Fragment) {

    @BindingAdapter(value = ["imageUrl", "imageRequestListener"], requireAll = false)
    fun bindImage(imageView: ImageView, url: String?, listener: RequestListener<Drawable?>?) {
        Glide.with(fragment).load(url).placeholder(R.drawable.ic_placeholder).listener(listener).into(imageView)
    }

    @BindingAdapter(value = ["imageUrl", "imageRequestListener"], requireAll = false)
    fun bindImage(cardView: CardView, url: String?, listener: RequestListener<Drawable?>?) {

        val target = object : SimpleTarget<Drawable>(800, 800) {
            override fun onResourceReady(resource: Drawable, transition: Transition<in Drawable>?) {
                cardView.setBackgroundDrawable(resource)
            }
        }

        Glide.with(fragment)
                .load(url)
                .centerInside()
                .placeholder(R.drawable.ic_placeholder)
                .listener(listener)
                .into(target)
    }

}