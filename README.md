Aptoide Technical Challenge
===========================================================

## Architect Pattern

I decided to use the MVVM Pattern since it clearly separates the application in layers.

##### Model
A single source of truth. In this case there is one data source but it could handle multiple datasources.
 
It has no knowledge on who is the UI and what it does. The model doesn't know who consumes the data.

##### ViewModel
To Observe changes in the data. It uses one or more repositories to fetch all the info and transforms it when needed.

Doesn't know about the UI and what it does.

##### View
Contains 0+ view models and observes the changes on each view model.
It uses databinding to update the UI with information that the view model returns.


## Testing

I should have implemented the following tests:
* UI Tests
* Local Unit Tests
* ViewModel Tests
* Repository Tests
* Webservice Tests


## Libraries
* [Android X Artifacts][androidx]
* [Android Architecture Components][arch]
* [Android Data Binding][data-binding]
* [Dagger 2][dagger2] for dependency injection
* [Retrofit][retrofit] for REST api communication with aptoide servers
* [RxJava][rxjava] and [RxAndroid][rxandroid] for reactive streams observing remote changes
* [Glide][glide] for image loading into recycler view items
* [Timber][timber] for logging
* [Espresso][espresso] for UI tests (Not Implemented)
* [Mockito][mockito] for mocking in tests (Not Implemented)


[mockwebserver]: https://github.com/square/okhttp/tree/master/mockwebserver
[androidx]: https://developer.android.com/jetpack/androidx
[arch]: https://developer.android.com/arch
[data-binding]: https://developer.android.com/topic/libraries/data-binding/index.html
[espresso]: https://google.github.io/android-testing-support-library/docs/espresso/
[dagger2]: https://google.github.io/dagger
[retrofit]: http://square.github.io/retrofit
[glide]: https://github.com/bumptech/glide
[timber]: https://github.com/JakeWharton/timber
[mockito]: http://site.mockito.org
[rxjava]: https://github.com/ReactiveX/RxJava
[rxandroid]: https://github.com/ReactiveX/RxJava
